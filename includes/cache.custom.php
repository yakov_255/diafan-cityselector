<?php

if (!defined('DIAFAN')) {
    $path = __FILE__;
    while (!file_exists($path . '/includes/404.php')) {
        $parent = dirname($path);
        if ($parent == $path)
            exit;
        $path = $parent;
    }
    include $path . '/includes/404.php';
}

/**
 * Cache
 *
 * Кэширование
 */
class Cache
{
    /**
     * Преобразует метку и название модуля для работы с кэшем
     * Отличается от оригинальной функции тем, что учитывает город
     *
     * @param string|array $name метка кэша
     * @param string $module название модуля
     * @return boolean true
     */
replace private function transform_param($name, $module)
{

    $cityId = '';
    if (isset($GLOBALS['diafan'])) {
        $cityId = $GLOBALS['diafan']->_cityselector->getPriceParamValueId();
    }

    if ($name) {
        if (!is_array($name)) {
            $this->name = md5($cityId . $name);
        } else {
            $this->name = md5($cityId . serialize($name));
        }
    } else {
        $this->name = $cityId;
    }
    if ($module) {
        $this->module = md5($module);
    } else {
        $this->module = '';
    }
    return true;
}
}

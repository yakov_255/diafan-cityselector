<?php
/**
 * Модель модуля «Методы оплаты»
 *
 * @package    DIAFAN.CMS
 * @author     diafan.ru
 * @version    6.0
 * @license    http://www.diafan.ru/license.html
 * @copyright  Copyright (c) 2003-2018 OOO «Диафан» (http://www.diafan.ru/)
 */

if (!defined('DIAFAN')) {
    $path = __FILE__;
    while (!file_exists($path . '/includes/404.php')) {
        $parent = dirname($path);
        if ($parent == $path)
            exit;
        $path = $parent;
    }
    include $path . '/includes/404.php';
}

class Cityselector_inc extends Diafan
{
    /**
     * Возращает список городов в виде ключ - навзание
     * @return array
     * @throws DB_exception
     */
    public function getList()
    {
        $list = DB::query_fetch_key_value("SELECT id, [name] FROM {cityselector} ORDER BY sort", 'id', 'name');

        return $list;
    }

    /**
     * Возращает ид значения характеристики "Тип цены" привязанный к текущему городу.
     * (Учитывая розницу / опт)
     */
    public function getPriceParamValueId()
    {
        // Получение id значения характеристики
        if (empty($this->cache["price_param_value_id"])) {

            if ($this->diafan->_users->id) {
                // Если пользователь идентифицирован
                // Попытаться получить оптовые цены

                $client_type_string = $this->diafan->configmodules("client_type", 'cityselector');
                $client_wholesale_value_string = $this->diafan->configmodules("client_wholesale_value", 'cityselector');

                // Получить ид характеристики типа пользователя
                if (empty($this->cache['price_param_user_type_id'])) {

                    // ид характеристики ента
                    $client_type = DB::query_result("select s.[name] from {users_param} u LEFT JOIN {users_param_element} p 
on u.id = p.param_id LEFT JOIN {users_param_select} s on p.value = s.id and s.param_id = u.id where u.[name] = '%s' AND p.element_id = %d",
                        $client_type_string, $this->diafan->_users->id);

                    if (!empty($client_type)) {
                        $this->cache['price_param_user_type_id'] = $client_type;
                    }

                }
                $client_type = $this->cache['price_param_user_type_id'];

                if ($client_type == $client_wholesale_value_string) {
                    $this->cache["price_param_value_id"] = $this->getWholesalePriceParamValueId();
                } else {
                    $this->cache["price_param_value_id"] = $this->getRetailPriceParamValueId();
                }

            } else {
                // Если пользователь не идентифицирован - показывать розничные цены
                $this->cache["price_param_value_id"] = $this->getRetailPriceParamValueId();
            }
        }
        return $this->cache["price_param_value_id"];
    }

    /**
     * Возращает ид значения характеристики "Тип цены" привязанный к текущему городу.
     * (Розничная цена)
     */
    public function getRetailPriceParamValueId()
    {
        $cityId = $this->getCity();
        if (empty($cityId))
            return false;

        return DB::query_result("SELECT retail_price_id FROM {cityselector} WHERE id=%d", $cityId['id']);
    }


    /**
     * Возращает ид значения характеристики "Тип цены" привязанный к текущему городу.
     * (Розничная цена)
     */
    public function getWholesalePriceParamValueId()
    {
        $cityId = $this->getCity();
        if (empty($cityId))
            return false;

        return DB::query_result("SELECT wholesale_price_id FROM {cityselector} WHERE id=%d", $cityId['id']);
    }

    /**
     * Переход на домен другого города
     * @throws Exception
     */
    public function redirect()
    {

        $city = $this->getCity();

        if ($city && $this->getSubdomain() != $city['subdomain']) {

            $url = getenv('REQUEST_URI');

            $host = getenv("HTTP_HOST");
            $hostExploded = explode('.', $host);

            $list = $this->getCityList();

            foreach ($list as $row) {
                if ($hostExploded[0] == $row['subdomain']) {
                    array_shift($hostExploded);
                    break;
                }
            }

            $newHost = ($city['subdomain'] != '' ? $city['subdomain'] . '.' : '') . implode('.', $hostExploded);

            if ($newHost != $host) {

                $this->diafan->redirect('http' . (IS_HTTPS ? "s" : '') . '://' . $newHost . $url);
                exit();
            }

        }
    }


    /**
     * Возращает id текущего города
     * (Обертка для кэша)
     * @param $values
     */
    public function getCity()
    {
        if (empty($this->cache['city'])) {

            $this->cache['city'] = $this->_getCity();

        }
        return $this->cache['city'];
    }

    /**
     * Возвращает id текущего города
     * Город определяется через куки. Если через куки не удалось - по домену
     * @return bool|mixed
     * @throws Exception
     */
    private function _getCity()
    {
        $values = $this->getCityList();

        // Попытка получить город из cookie
        if (!empty($_COOKIE['city_id'])) {

            foreach ($values as $key => $row) {
                if ($row['id'] == $_COOKIE['city_id']) {
                    return $row;
                }
            }
        }

        // Попытка получить город из поддомена
        $subDomain = $this->getSubdomain();
        foreach ($values as $key => $row) {
            if ($row['subdomain'] == $subDomain) {
                return $row;
            }
        }
        return false;
    }

    /**
     * Возращает текущий поддомен
     * Требует указания базового домена в настройках
     * @return mixed
     * @throws Exception
     */
    private function getSubdomain()
    {
        if (!$this->diafan->configmodules("domain", 'cityselector')) {
            throw new Exception("Не указан домен в настройках модуля 'Города'");
        }

        $subdomain = $this->diafan->configmodules("domain", 'cityselector');
        $domain = $_SERVER['HTTP_HOST'];
        // Заменяем в строке HTTP_HOST известный нам домен на пустую строку и убираем точку
        return str_replace('.', '', str_replace($subdomain, '', $domain));
    }

    /**
     * @return array
     */
    private function getCityList()
    {
        // Получение списка городов
        $values = array();
        foreach (DB::query_fetch_all("SELECT id, [name], subdomain FROM {cityselector} WHERE trash='0' ORDER BY sort") as $key => $row) {
            $values[$row['id']] = $row;
        }
        return $values;
    }

    /**
     * СОздание фильтра товаров с учетом цены
     * @return array
     * @throws DB_exception
     */

    public function GetPriceFilter()
    {
        $result = array('where' => '', 'from' => '');

        // Получение id характеристики
        if (empty($this->cache["price_param_id"])) {
            $this->cache["price_param_id"] = DB::query_result("SELECT id FROM {shop_param} WHERE [name]='%h' AND (type='multiple' OR type='select') AND trash='0' LIMIT 1", 'Тип цены');
        }
        $price_param_id = $this->cache["price_param_id"];

        // Не применять фильтр в случае ошибки
        if (empty($price_param_id))
            return $result;

        // Получить ид значения характеристики
        $value_id = $this->getPriceParamValueId();

        // Не применять фильтр в случае ошибки
        if (empty($value_id))
            return $result;

        $result['from'] = ' JOIN {shop_price_param} pp on p.id = pp.price_id ';
        $result['where'] = ' pp.param_id=' . $price_param_id . ' AND pp.param_value=' . $value_id . ' AND ';

        return $result;
    }
}
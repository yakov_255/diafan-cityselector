<?php
/**
 * Установка модуля
 *
 * @package    DIAFAN.CMS
 * @author     diafan.ru
 * @version    6.0
 * @license    http://www.diafan.ru/license.html
 * @copyright  Copyright (c) 2003-2018 OOO «Диафан» (http://www.diafan.ru/)
 */

if (!defined('DIAFAN')) {
    $path = __FILE__;
    while (!file_exists($path . '/includes/404.php')) {
        $parent = dirname($path);
        if ($parent == $path)
            exit;
        $path = $parent;
    }
    include $path . '/includes/404.php';
}


class Cityselector_install extends Install
{


    /**
     * @var string название
     */
    public $title = "Города";

    /**
     * @var array таблицы в базе данных
     */
    public $tables = array(
        array(
            "name" => "cityselector",
            "comment" => "Города",
            "fields" => array(
                array(
                    "name" => "id",
                    "type" => "SMALLINT(5) UNSIGNED NOT NULL AUTO_INCREMENT",
                    "comment" => "идентификатор",
                ),
                array(
                    "name" => "name",
                    "type" => "VARCHAR(50) NOT NULL DEFAULT ''",
                    "comment" => "Название города",
                    "multilang" => true,
                ),
                array(
                    "name" => "retail_price_id",
                    "type" => "INT(11) UNSIGNED NOT NULL DEFAULT 0",
                    "comment" => "идентификатор Значения типа цены (характеристики товара) для оптовых цен",
                ),
                array(
                    "name" => "wholesale_price_id",
                    "type" => "INT(11) UNSIGNED NOT NULL DEFAULT 0",
                    "comment" => "идентификатор Значения типа цены (характеристики товара) для розничных цен",
                ),
                array(
                    "name" => "phone",
                    "type" => "TEXT DEFAULT NULL",
                    "comment" => "Телефон",
                ),
                array(
                    "name" => "address",
                    "type" => "TEXT DEFAULT NULL",
                    "comment" => "Адрес",
                ),
                array(
                    "name" => "subdomain",
                    "type" => "VARCHAR(11) NOT NULL DEFAULT ''",
                    "comment" => "Поддомен для переадресации",
                ),
                array(
                    "name" => "trash",
                    "type" => "ENUM('0', '1') NOT NULL DEFAULT '0'",
                    "comment" => "запись удалена в корзину: 0 - нет, 1 - да",
                ),
                array(
                    "name" => "sort",
                    "type" => "INT(11) UNSIGNED NOT NULL DEFAULT '0'",
                    "comment" => "подрядковый номер для сортировки",
                ),
            ),
            "keys" => array(
                "PRIMARY KEY (id)",
            ),
        ),
    );

    /**
     * @var array SQL-запросы
     */
    public $sql = array(
        "cityselector" => array(
            array(
                "name" => array('Екатеринбург'),
            ), array(
                "name" => array('Тюмень'),
            ),
            array(
                "name" => array('Омск'),
            ),
            array(
                "name" => array('Новосибирск'),
            ),
            array(
                "name" => array('Ижевск'),
            ),
            array(
                "name" => array('Уфа'),
            ),
        )
    );


    /**
     * Выполняет действия при установке модуля
     *
     * @return void
     */
    protected function action()
    {
        $this->config[] = array(
            "name" => "domain",
            "value" => $_SERVER['HTTP_HOST'],
        );
    }

    /**
     * @var array настройки
     */
    public $config = array(
        array(
            "name" => "client_type",
            "value" => "Тип клиента",
        ),
        array(
            "name" => "client_wholesale_value",
            "value" => "Оптовый",
        ),
    );

    /**
     * @var array записи в таблице {modules}
     */
    public $modules = array(
        array(
            "name" => "cityselector",
            "admin" => true, // Есть админка
            "site" => true, // Может отображать код на сайте (работает shop_block)
            "site_page" => true,
        ),
    );

    /**
     * @var array меню административной части
     */
    public $admin = array(
        array(
            "name" => "Города",
            "rewrite" => "cityselector",
            "group_id" => 4,
            "sort" => 14,
            "act" => true,
            "children" => array(
                array(
                    "name" => "Города",
                    "rewrite" => "cityselector",
                    "sort" => 1,
                    "act" => true,
                ),
                array(
                    "name" => "Настройки",
                    "rewrite" => "cityselector/config",
                ),
            )
        ),
    );
}

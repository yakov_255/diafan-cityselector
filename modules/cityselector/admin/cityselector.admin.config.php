<?php

if (!defined('DIAFAN')) {
    $path = __FILE__;
    while (!file_exists($path . '/includes/404.php')) {
        $parent = dirname($path);
        if ($parent == $path)
            exit;
        $path = $parent;
    }
    include $path . '/includes/404.php';
}


class Cityselector_admin_config extends Frame_admin
{
    /**
     * @var array поля в базе данных для редактирования
     */
    public $variables = array(
        'base' => array(
            'domain' => array(
                'type' => 'text',
                'name' => 'Домен (example.com) - базовый домен для переадресации'
            ),

            'client_type' => array(
                'type' => 'text',
                'name' => 'Название параметра пользователя (Тип клиента)'
            ),
            'client_wholesale_value' => array(
                'type' => 'text',
                'name' => 'Значение параметра для оптовых цен (Оптовый)'
            ),
        )
    );

    /**
     * @var array настройки модуля
     */
    public $config = array(
        'config' // файл настроек модуля
    );


}
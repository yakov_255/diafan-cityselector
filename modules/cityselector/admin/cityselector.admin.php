<?php
/**
 * Редактирование методов оплаты
 *
 * @package    DIAFAN.CMS
 * @author     diafan.ru
 * @version    6.0
 * @license    http://www.diafan.ru/license.html
 * @copyright  Copyright (c) 2003-2018 OOO «Диафан» (http://www.diafan.ru/)
 */

if (!defined('DIAFAN')) {
    $path = __FILE__;
    while (!file_exists($path . '/includes/404.php')) {
        $parent = dirname($path);
        if ($parent == $path)
            exit;
        $path = $parent;
    }
    include $path . '/includes/404.php';
}

/**
 * Shop_admin_cityselector
 */
class Cityselector_admin extends Frame_admin
{
    /**
     * @var string таблица в базе данных
     */
    public $table = 'cityselector';

    /**
     * @var array поля в базе данных для редактирования
     */
    public $variables = array(
        'main' => array(
            'name' => array(
                'type' => 'text',
                'name' => 'Название города',
                'multilang' => true,
            ),
            'subdomain' => array(
                'type' => 'text',
                'name' => 'Поддомен',
                'help' => 'Если указан - город будет определяется по поддомену. И наоборот - при выборе города будет выполнена переаддресация. Указывать без точки. Например moscow.',
            ),
            'hr1' => 'hr',
            'retail_price_id' => array(
                'type' => 'function',
                'name' => 'Розничная цена',
                'help' => 'Значения характеристики "Тип цены".',
            ),
            'wholesale_price_id' => array(
                'type' => 'function',
                'name' => 'Оптовая цена',
                'help' => 'Значения характеристики "Тип цены".',
            ),
            'hr2' => 'hr',
            'phone' => array(
                'type' => 'editor',
                'name' => 'Телефон',
                'help' => 'Для отобажения на сайте используйте код <insert name="show_phone" module="cityselector">',
            ),
            'address' => array(
                'type' => 'editor',
                'name' => 'Адрес',
                'help' => 'Для отобажения на сайте используйте код <insert name="show_phone" module="cityselector">',
            ),
            'dynamic' => array(
                'type' => 'function',
                'name' => 'Динамические блоки',
            ),
        ),
    );

    /**
     * @var array поля в списка элементов
     */
    public $variables_list = array(
        'checkbox' => '',
        'sort' => array(
            'name' => 'Сортировка',
            'type' => 'numtext',
            'sql' => true,
            'fast_edit' => true,
        ),
        'name' => array(
            'name' => 'Название'
        ),
        'retail_price_id' => array(
            'name' => 'Розничная цена',
            'type' => 'select',
            'sql' => true,

        ),
        'wholesale_price_id' => array(
            'name' => 'Оптовая цена',
            'sql' => true,
        ),
        'actions' => array(
            'trash' => true,
        ),
    );

    /**
     * Выводит ссылку на добавление
     * @return void
     */
    public function show_add()
    {
        $this->diafan->addnew_init('Добавить');
    }

    /**
     * Выводит список методов оплаты
     * @return void
     */
    public function show()
    {
        $this->diafan->list_row();
    }

    /**
     * Отображение значения в списке
     * @param $row
     * @param $var
     */
    public function list_variable_retail_price_id($row, $var)
    {
        $this->show_list_value($row['retail_price_id']);
    }

    /**
     * Отображение значения в списке
     * @param $row
     * @param $var
     */
    public function list_variable_wholesale_price_id($row, $var)
    {
        $this->show_list_value($row['wholesale_price_id']);
    }

    public function edit_variable_retail_price_id()
    {
        $this->show_price_value_select("retail_price_id");
    }

    public function save_variable_retail_price_id()
    {
        $this->diafan->set_query("retail_price_id='%d'");
        $this->diafan->set_value($_POST['retail_price_id']);
    }

    public function edit_variable_wholesale_price_id()
    {
        $this->show_price_value_select("wholesale_price_id");
    }

    public function save_variable_wholesale_price_id()
    {
        $this->diafan->set_query("wholesale_price_id='%d'");
        $this->diafan->set_value($_POST['wholesale_price_id']);
    }


    private function show_price_value_select($param_name)
    {
        $rows = $this->get_rows();

        echo '		
		<div class="unit">
			<div class="infofield">
				' . $this->diafan->variable_name() . '
			</div>
			<select name="' . $param_name . '"><option value="">-</option>';
        foreach ($rows as $key => $name) {
            echo '<option value="' . $key . '"' . ($this->diafan->value == $key ? ' selected' : '') . '>' . $name . '</option>';
        }
        echo '</select>
		</div>';
    }

    /**
     * Получает список всех цен
     *
     * @return array
     */
    private function get_rows()
    {
        // Получение ИД характеристики

        $name = "Тип цены";

        $param_id = DB::query_result("SELECT id FROM {shop_param} WHERE [name]='%h' AND (type='multiple' OR type='select') AND trash='0' LIMIT 1", $name);

        if (empty($param_id))
            return array();

        $values = DB::query_fetch_key_value("SELECT id, [name] FROM {shop_param_select} WHERE param_id=%d AND trash='0'", $param_id, "id", 'name');

        return $values;
    }

    /**
     * @param $row
     */
    private function show_list_value($id)
    {
        $rows = $this->get_rows(); // array(key => value)
        foreach ($rows as $key => $name) {
            if ($id == $key) {
                echo '<div class="text">' . $name . '</div>';
                break;
            }
        }
    }


}

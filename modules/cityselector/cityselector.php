<?php

if (!defined('DIAFAN')) {
    $path = __FILE__;
    while (!file_exists($path . '/includes/404.php')) {
        $parent = dirname($path);
        if ($parent == $path)
            exit;
        $path = $parent;
    }
    include $path . '/includes/404.php';
}

/**
 *
 */
class Cityselector extends Controller
{
    private static $once = false;


    /**
     * Отображение телефона для текущего города
     * Если телефон не указан - ничего выведено не будет
     * @param $attributes
     */
    public function show_phone($attributes)
    {
        $this->getFieldValue('phone');
    }

    /**
     * Отображение адреса для текущего города
     * Если адрес не указан - ничего выведено не будет
     * @param $attributes
     */
    public function show_address($attributes)
    {
        $this->getFieldValue('address');
    }


    /**
     * Шаблонная функция: выводит содержимое динамического блока, номер которой передан в виде атрибута id.
     *
     * @param array $attributes атрибуты шаблонного тега
     * id - идентификатор динамического блока
     * defer - маркер отложенной загрузки шаблонного тега: **event** – загрузка контента только по желанию пользователя при нажатии кнопки "Загрузить", **emergence** – загрузка контента только при появлении в окне браузера клиента, **async** – асинхронная (одновременная) загрузка контента совместно с контентом шаблонных тегов с тем же маркером, **sync** – синхронная (последовательная) загрузка контента совместно с контентом шаблонных тегов с тем же маркером, по умолчанию загрузка контента только по желанию пользователя
     * defer_title - текстовая строка, выводимая на месте появления загружаемого контента с помощью отложенной загрузки шаблонного тега
     * template - шаблон тега (файл modules/site/views/site.view.show_dynamic_**template**.php; по умолчанию шаблон modules/site/views/site.view.show_dynamic.php)
     *
     * @return void
     */
    public function show_dynamic($attributes)
    {
        $attributes = $this->get_attributes($attributes, 'id', 'template');

        $city = $this->diafan->_cityselector->getCity();

        $attributes["id"] = intval($attributes["id"]);

        $model = $this->includeClass('site', 'model') ;

        $result = $model->show_dynamic($attributes["id"], $city['id'], 'cityselector', 'element');

        if (!$result)
            return;

        $result["attributes"] = $attributes;

        echo $this->diafan->_tpl->get('show_dynamic', 'site', $result, $attributes["template"]);
    }

    private function includeClass($module, $name)
    {
        // Код из controller.php -> __get() -> model (54 - 65)
        if (Custom::exists('modules/' . $module . '/' . $module . '.' . $name . '.php')) {
            Custom::inc('modules/' . $module . '/' . $module . '.' . $name . '.php');
            $class = ucfirst($module) . '_' . $name;
            $this->cache[$name . '_' . $module] = new $class($this->diafan);
        } else {
            throw new Controller_exception($this->diafan->_('Файл %s не существует.', false, 'modules/' . $module . '/' . $module . '.' . $name . '.php'));
        }
        return $this->cache[$name . '_' . $module];
    }


    /**
     * Отображение выпадающего списка выбора города
     * Так же этот блок выводт код всплывающего окна
     * Блок можно использовать на странице несколько раз
     * При этом JS код будет отображен единожды
     * @param $attributes
     * @throws Exception
     */
    public function show_block($attributes)
    {
        if (!$this->diafan->configmodules("domain", 'cityselector')) {
            throw new Exception("Не указан домен в настройках модуля 'Города'");
        }
        // Получение списка городов
        $values = array();
        foreach (DB::query_fetch_all("SELECT id, [name], subdomain FROM {cityselector} WHERE trash='0' ORDER BY sort") as $key => $row) {
            $values[$row['id']] = $row;
        }
        // Текущий город
        $currentCityId = $_COOKIE['city_id'];

        // Если первый запуск
        if (!self::$once) {

            // Если город не выбран
            if (empty($currentCityId)) {
                $domainCity = $this->diafan->_cityselector->getCity();
                ?>
                <div class="modal_city" id="modal_city">
                    <?php
                    // Если найден город по домену
                    if ($domainCity) {
                        // Уточнить у пользователя прафильный ли город
                        ?>
                        <div class="title_modal">Ваш город <?= $domainCity['name'] ?>?</div>
                        <button data-fancybox-close="" onclick="cityselector.setCity(<?= $domainCity['id'] ?>)">
                            Сохранить
                        </button>
                        <br>
                        <br>
                        <p>Выбрать другой город</p>
                        <?php
                    } else {
                        // Предложить выбрать город
                        ?>
                        <div class="title_modal">Выберите город</div>
                        <?php
                    }
                    ?>

                    <div class="form_city">
                        <?php
                        // Выпадающий список выбора города
                        $this->printList($values);
                        ?>
                        <button data-fancybox-close="">Сохранить</button>
                    </div>
                </div>
                <script type="application/javascript">
                    // Ожидание загрузки Jquery и FancyBox

                    (function () {
                        var f = function () {
                            if (typeof $ !== "undefined" && $.fancybox) {
                                const e = $(".modal_city");
                                e.fancybox();
                                e.eq(0).trigger('click');
                            } else {
                                console.log("wait for fancybox");
                                setTimeout(f, 50);
                            }
                        };
                        f();
                    })();


                </script>
                <?php
            }
            ?>
            <script type="application/javascript">

                if (typeof cityselector === 'undefined') {
                    var cityselector = {
                        // Домен сайта для записи в куки
                        domain: '<?= $this->diafan->configmodules("domain", 'cityselector') ?>',

                        // Запись города
                        setCity: function (cityId) {
                            // Set cookie
                            const date = new Date(new Date().getTime() + (1000 * 86400 * 365 * 10)); // Ten years
                            document.cookie = "city_id=" + cityId + "; path=/; expires=" + date.toUTCString() + ";domain=" + cityselector.domain;
                        },

                        // Смена города
                        changeCity: function (elem) {
                            cityselector.setCity(elem.options[elem.selectedIndex].value);
                            // Reload page
                            location.reload(true); // Reload.  Ignore cache
                        }
                    }
                }
            </script>
            <?php
        }
        $this->printList($values);

        self::$once = true;
    }


    /**
     * Вывод выпадающего списка городов
     * @param $values
     * @param $currentCity
     */
    private function printList($values)
    {

        $currentCity = $this->diafan->_cityselector->getCity();

        // Вывод списка
        echo '<select onchange="cityselector.changeCity(this)" name="cityselector">';
        foreach ($values as $key => $row) {
            echo '<option value="' . $key . '"' . ($currentCity['id'] == $key ? ' selected' : '') . '>' . $row['name'] . '</option>';
        }
        echo '</select>';
    }

    /**
     * Возращает значение поля для текущего города
     * @param $fieldName
     */
    private function getFieldValue($fieldName)
    {
        $city = $this->diafan->_cityselector->getCity();

        if (!$city)
            return;

        $result = DB::query_result("SELECT $fieldName FROM {cityselector} WHERE id=%d", $city['id']);

        echo $result;
    }

}

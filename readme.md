##Модуль управления городами

###Подлючение списка выбора города:

    <insert name="show_block" module="cityselector">


### Отображение телефона и адреса  
Телефон:  

    <insert name="show_phone" module="cityselector">
Город:
  
    <insert name="show_address" module="cityselector">

### Отображение динамических блоков:
Позволяет вывести любую информацию в зависимости от города.  
Как использовать:  
В модуле "Страницы сайта" в разделе "Динамические блоки" добавьте новый блок  
Укажите модуль "Города", и "Прикрепить в модуле к" выберите "элементам"  
После создания динамического блока, перейдите в модуль "Города" и откройте существующий или создайте новый город.  
На странице редактирования будет отображен динамический блок.  
Заполните блок для каждого города  
Затем подключите вывод блока в шаблоне  

    <insert name="show_dynamic" module="cityselector" id="3">
    
Где id = номер блока из модуля "Страницы сайта"



## Установка

###Для работы переадресации добавить в includes/init.php
После  

    $this->module();  
Добавить

  // Добавлено для модуля cityselector (Перенаправление на поддомен при смене города)
    $this->module->diafan->_cityselector->redirect();  

###Для правильного отображения цен нужно установить фильтр цен.
Если вам нужно внести изменения в файл modules/shop/inc/shop.inc.php,  
Вам необходимо добавить фильтр цен вручную  
Для этого в файле  
modules/shop/inc/shop.inc.php  
В функции  
price_get_all
Перед  

    $all_rows = DB::query_fetch_key_array(  
Добавить
  
	// Добавлено для модуля cityselector (Разные цены в разных городах)
    $price_filter = $this->diafan->_cityselector->GetPriceFilter();  
после слова FROM добавить $price_filter['from']  
в условие where добавить $price_filter['where']  

Пример:
<pre>
    <b>$price_filter = $this->diafan->_cityselector->GetPriceFilter();</b>

    $all_rows = DB::query_fetch_key_array(
        "SELECT p.* FROM {shop_price} AS p"
        <b>. $price_filter['from']</b>
        . " LEFT OUTER JOIN {shop_discount} AS d ON p.discount_id=d.id"
        . " WHERE <b>" . $price_filter['where'] . "</b> p.good_id IN (%s) AND p.trash='0'"
        . " AND p.currency_id=0"
        . " AND p.role_id" . ($role_id ? " IN (0," . $role_id . ")" : "=0")
        . " AND p.date_start<=%d AND (p.date_finish=0 OR p.date_finish>=%d)"
        . " ORDER BY p.price ASC, id ASC",
        implode(",", array_keys($prepare_all)), time(), time(),
        "good_id");
</pre>

Если вам нужна функция shop.action -> wait (добавление в список отложенных)    
То вам необходимо добавить фильтр.  
Для этого в файле  
modules/shop/shop.action.php   
В функции wait, после  

    foreach ($rows_param as $row_param) {
Добавить проверку цены:

    // Добавлено для модуля cityselector (Разные цены в разных городах)
    if ($row_param['name'] == 'Тип цены') {
            $params[$row_param["id"]] = $this->diafan->_cityselector->getPriceParamValueId();
            continue;
    }  
Должен получится прммерно следующий код:

    foreach ($rows_param as $row_param) {
        // Добалвено: модуль Выбор города - Фильтр цен по городу
        if ($row_param['name'] == 'Тип цены') {
            $params[$row_param["id"]] = $this->diafan->_cityselector->getPriceParamValueId();
            continue;
        }

        if (empty($_POST["param" . $row_param["id"]])) {
            $this->result["errors"]["waitlist"] = $this->diafan->_('Пожалуйста, выберите %s.', false, $row_param["name"]);
            return;
        } else {
            $params[$row_param["id"]] = $this->diafan->filter($_POST, "int", "param" . $row_param["id"]);
        }
    }
    
Если вам нужна функция *cart.action -> one_click* (Покупка в 1 клик)    
То вам необходимо добавить фильтр.  
Для этого в файле  
modules/shop/shop.action.php   
В функции one_click, после     
 
    if (empty($_POST["param" . $row_param["id"]])) {
Добавить

    // Добавлено для модуля cityselector (Разные цены в разных городах)
    if ($row_param['name'] == 'Тип цены') {
        $params[$row_param["id"]] = $this->diafan->_cityselector->getPriceParamValueId();
        continue;
    } 
